import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_flutter/core/utils/app_color.dart';

class BuildFilter extends StatelessWidget {
  const BuildFilter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
      height: 40.h,
      width: 40.w,
      decoration: BoxDecoration(
          color: AppColor.primaryColor,
          borderRadius: BorderRadius.circular(5)),
      child: Icon(
        Icons.filter_list,
        color: Colors.white,
      ),
    );
  }
}
