import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_flutter/core/utils/app_color.dart';
import 'package:task_flutter/core/utils/components/components.dart';
import 'package:task_flutter/core/utils/const/strings_const/string_const.dart';
import 'package:task_flutter/moduels/taske/service/video_app_cubit.dart';
enum MenuItemButton{
  item1,item2,item3,item4
}
class BuildHeader extends StatelessWidget {
  const BuildHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit=VideoAppCubit.get(context);
    return Row(
      children: [
        CircleAvatar(
          backgroundColor: AppColor.primaryColor,
          radius: 30,
          child: const CircleAvatar(
            radius: 27,
            backgroundImage: NetworkImage(
                'https://img.freepik.com/premium-photo/portrait-smiling-young-casual-brunette-woman_171337-29897.jpg'),
          ),
        ),
        SizedBox(
          width: 8.w,
        ),
        Components.defaultText(
          text: sayWelcome,
          fontSize: 13,
        ),
        const Spacer(),
        Card(
          elevation: 3,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40),
          ),
          child:  Padding(
            padding: EdgeInsets.all(5.0),
            child:  PopupMenuButton
              (
              child: const Icon(Icons.more_horiz,color: Colors.grey,),
              onSelected: (value){
                if(value==MenuItemButton.item1) {
                  cubit.pageSize=5;
                }
                if(value==MenuItemButton.item2) {
                  cubit.pageSize=10;
                }
                if(value==MenuItemButton.item3) {
                  cubit.pageSize=15;
                }
                if(value==MenuItemButton.item4) {
                  cubit.pageSize=20;
                }
              },
              itemBuilder: (context)=>[
                PopupMenuItem(value: MenuItemButton.item1,child: Components.defaultText(text: fiveVideo, fontSize: 12.sp,),),
                PopupMenuItem(value: MenuItemButton.item2,child: Components.defaultText(text: tenVideo, fontSize: 12.sp,),),
                PopupMenuItem(value: MenuItemButton.item3,child: Components.defaultText(text: fifteenVideo, fontSize: 12.sp,),),
                PopupMenuItem(value: MenuItemButton.item4,child: Components.defaultText(text: twentyVideo, fontSize: 12.sp,),),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
