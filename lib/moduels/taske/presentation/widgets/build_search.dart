import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_flutter/core/utils/app_color.dart';

class BuildSearch extends StatelessWidget {
  const BuildSearch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: 40.h,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: AppColor.textFormColor,
        ),
        child: TextFormField(
          cursorColor: Colors.black,
          cursorHeight: 30,
          decoration: InputDecoration(
            suffixIcon: Icon(Icons.search),
            border: InputBorder.none,
            hintText: 'Search For Courses',
            hintStyle: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.normal,
              fontSize: 10.sp,
            ),
            contentPadding: EdgeInsets.only(left: 10.w),
          ),
        ),
      ),
    );
  }
}
