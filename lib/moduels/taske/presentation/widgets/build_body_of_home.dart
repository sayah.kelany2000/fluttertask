import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:task_flutter/core/utils/components/components.dart';
import 'package:task_flutter/core/utils/const/strings_const/string_const.dart';
import 'package:task_flutter/moduels/taske/presentation/widgets/build_filter.dart';
import 'package:task_flutter/moduels/taske/presentation/widgets/build_list_of_videos.dart';
import 'package:task_flutter/moduels/taske/presentation/widgets/build_search.dart';
import 'package:task_flutter/moduels/taske/service/video_app_cubit.dart';

import 'build_header.dart';

class BuildBodyOfHomeScreen extends StatelessWidget {
  BuildBodyOfHomeScreen({Key? key}) : super(key: key);
  RefreshController refreshController = RefreshController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 16.w,
        top: 45.h,
        right: 16.w,
        bottom: 10.h,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const BuildHeader(),
          SizedBox(
            height: 14.h,
          ),
          Components.defaultText(
            text: title,
            fontSize: 14,
          ),
          SizedBox(
            height: 11.h,
          ),
          Components.defaultText(
            text: coursesHere,
            fontSize: 14,
          ),
          SizedBox(
            height: 11.h,
          ),
          Row(
            children: [
              const BuildSearch(),
              SizedBox(
                width: 5.w,
              ),
              const BuildFilter(),
            ],
          ),
          Expanded(
            child: BlocBuilder<VideoAppCubit, VideoAppState>(
              builder: (context, state) {
                if (state is GetAllVideoErrorState) {
                  return Center(
                    child: Components.defaultText(
                        text: '${state.errorModel!.message}', fontSize: 16.sp),
                  );
                }
                if (VideoAppCubit.get(context).listOfVideosModel != null) {
                  return SmartRefresher(
                    controller: refreshController,
                    enablePullDown: false,
                    enablePullUp: true,
                    onLoading: () {
                      VideoAppCubit.get(context).getAllVideos();
                      if (state is GetAllVideoSuccessState) {
                        refreshController.loadComplete();
                      } else {
                        refreshController.loadFailed();
                      }
                    },
                    child: ListView.separated(
                      itemBuilder: (context, index) {
                        return BuildListOfVideos(
                          index: index,
                        );
                      },
                      separatorBuilder: (context, index) => SizedBox(
                        height: 10.h,
                      ),
                      itemCount: VideoAppCubit.get(context)
                          .listOfVideosModel!
                          .data
                          .length,
                    ),
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
