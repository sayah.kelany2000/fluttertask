import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_flutter/core/network/end_points.dart';
import 'package:task_flutter/core/utils/app_color.dart';
import 'package:task_flutter/core/utils/components/components.dart';
import 'package:task_flutter/core/utils/const/strings_const/string_const.dart';
import 'package:task_flutter/moduels/taske/presentation/screens/video_screen.dart';
import 'package:task_flutter/moduels/taske/service/video_app_cubit.dart';

class BuildListOfVideos extends StatelessWidget {
  BuildListOfVideos({Key? key, required this.index}) : super(key: key);
  int index;

  @override
  Widget build(BuildContext context) {
    final cubit = VideoAppCubit.get(context).listOfVideosModel!;
    return GestureDetector(
      onTap: (){
        VideoAppCubit.get(context).detailsModel=null;
        VideoAppCubit.get(context).getVideoDetails(cubit.data[index].id);
        Navigator.push(context, MaterialPageRoute(builder: (context)=>VideoScreen()));
      },
      child: Card(
        color: Colors.white,
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Components.cachedNetworkImage(
                imageUrl: '$urlPhotos${cubit.data[index].coverImage}',
                placeHolder: Container(
                  width: double.infinity,
                  height: 115.h,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  child: Center(
                    child: Components.defaultText(
                      text: 'Loading',
                      fontSize: 12.sp,
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                  ),
                ),
                height: 115.h,
                errorWidget: Container(
                  width: double.infinity,
                  height: 115.h,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  child: Center(
                    child: Components.defaultText(
                      text: 'Error',
                      fontSize: 12.sp,
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius:const BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                  ),
                ),),
            Padding(
              padding: EdgeInsets.only(
                  left: 13.w, right: 16.w, bottom: 12.h, top: 12.h),
              child: Row(
                children: [
                  CircleAvatar(
                    backgroundColor: AppColor.primaryColor,
                    radius: 15,
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Components.defaultText(
                        text: '${cubit.data[index].name}',
                        fontSize: 10,
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      Components.defaultText(
                        text:
                            '${DateTime.now().difference(DateTime.parse(cubit.data[index].createAt)).inHours > 24 ? DateTime.now().difference(DateTime.parse(cubit.data[index].createAt)).inDays : DateTime.now().difference(DateTime.parse(cubit.data[index].createAt)).inHours}${DateTime.now().difference(DateTime.parse(cubit.data[index].createAt)).inHours > 24 ? ' Days' : 'H'} AGO',
                        fontSize: 9,
                      ),
                    ],
                  ),
                  const Spacer(),
                  Container(
                    height: 22.h,
                    width: 50.w,
                    decoration: BoxDecoration(
                        color: AppColor.primaryColor,
                        borderRadius: BorderRadius.circular(15)),
                    child: Center(
                      child: Components.defaultText(
                          text: '02:30', fontSize: 10, color: Colors.white),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
