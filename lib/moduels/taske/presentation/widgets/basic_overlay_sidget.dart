import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_flutter/core/utils/components/components.dart';
import 'package:task_flutter/moduels/taske/presentation/screens/landscape_played_screen.dart';
import 'package:video_player/video_player.dart';

class BasicOverlayWidget extends StatelessWidget {
  BasicOverlayWidget({Key? key, required this.controller,required this.isLand}) : super(key: key);
  VideoPlayerController controller;
  bool isLand;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        controller.value.isPlaying ? controller.pause() : controller.play();
      },
      child: Stack(
        children: [
          buildPlay(),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Row(
              children: [
                SizedBox(
                  width: 6.w,
                ),
                ValueListenableBuilder(
                    valueListenable: controller,
                    builder: (context, value, child) {
                      return Components.defaultText(
                          text: videoDuration(value.position),
                          fontSize: 10.sp,
                          color: Colors.white);
                    }),
                SizedBox(
                  width: 3.w,
                ),
                Expanded(
                  child: buildIndicator(),
                ),
                SizedBox(
                  width: 3.w,
                ),
                Components.defaultText(
                  text:
                  '${controller.value.duration.inSeconds < 59 ?'00:':''}${controller.value.duration.inSeconds > 59 ? controller.value.duration.inMinutes :controller.value.duration.inSeconds}',
                  fontSize: 10.sp,
                  color: Colors.white,
                ),
                if(!isLand)
                IconButton(onPressed: (){

                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => LandscapePlayedScreen(
                            controller: controller)));

                }, icon: Icon(Icons.fullscreen),color: Colors.white,),
                SizedBox(
                  width: 6.w,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String videoDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    final hours = twoDigits(duration.inHours);
    final minutes = twoDigits(duration.inMinutes.remainder(60));
    final seconds = twoDigits(duration.inSeconds.remainder(60));
    return [
      if (duration.inHours > 0) hours,
      minutes,
      seconds,
    ].join(':');
  }

  Widget buildIndicator() =>
      VideoProgressIndicator(controller, allowScrubbing: true);

  Widget buildPlay() => controller.value.isPlaying
      ? Container()
      : Container(
          alignment: Alignment.center,
          child: Icon(
            Icons.play_arrow,
            color: Colors.white,
            size: 80,
          ),
        );
}
