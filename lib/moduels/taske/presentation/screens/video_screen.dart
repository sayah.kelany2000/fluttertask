import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:task_flutter/core/utils/components/components.dart';
import 'package:task_flutter/moduels/taske/presentation/widgets/basic_overlay_sidget.dart';
import 'package:task_flutter/moduels/taske/service/video_app_cubit.dart';
import 'package:video_player/video_player.dart';

class VideoScreen extends StatelessWidget {
  const VideoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = VideoAppCubit.get(context);
    return BlocBuilder<VideoAppCubit, VideoAppState>(
      builder: (context, state) {
        return Scaffold(
          body: SafeArea(
              child: cubit.detailsModel != null
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        cubit.controller != null &&
                                cubit.controller!.value.isInitialized
                            ? Stack(
                                children: [
                                  Container(
                                    height: 205.h,
                                    child: VideoPlayer(cubit.controller!),
                                  ),

                                  Positioned.fill(
                                      child: BasicOverlayWidget(isLand: false,
                                    controller: cubit.controller!,
                                  )),
                                  Positioned(
                                    top: 10.h,
                                    left: 10.w,
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.arrow_back,
                                          color: Colors.white,
                                          size: 20,
                                        ),
                                        SizedBox(
                                          width: 6.w,
                                        ),
                                        GestureDetector(
                                          onTap: (){
                                            Navigator.pop(context);
                                          },
                                          child: Components.defaultText(
                                            text: 'back',
                                            fontSize: 12.sp,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              )
                            : Container(
                                color: Colors.black,
                                height: 205.h,
                                child: const Center(
                                  child: const CircularProgressIndicator(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                        SizedBox(height: 16.6.h),
                        Padding(
                          padding: EdgeInsets.only(left: 12.w, right: 12.w),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Components.defaultText(
                                text: '${cubit.detailsModel!.dataDetails.name}',
                                fontSize: 12.sp,
                              ),
                              SizedBox(
                                height: 6.sp,
                              ),
                              Row(
                                children: [
                                  Components.defaultText(
                                    text: DateFormat('yyyy').format(
                                        DateTime.parse(cubit.detailsModel!
                                            .dataDetails.createAt)),
                                    fontSize: 10.sp,
                                  ),
                                  SizedBox(
                                    width: 3.w,
                                  ),
                                  Container(
                                    height: 10.h,
                                    width: 1,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    width: 3.w,
                                  ),
                                  RatingBar.builder(
                                    minRating: 1,
                                    initialRating: double.tryParse(
                                        cubit.detailsModel!.dataDetails.rate)!,
                                    onRatingUpdate: (r) {},
                                    itemSize: 15,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return const Icon(
                                        Icons.star,
                                        color: Colors.amber,
                                      );
                                    },
                                  )
                                ],
                              ),
                              Components.defaultText(
                                text:
                                    '${cubit.detailsModel!.dataDetails.description}',
                                fontSize: 11.sp,
                              )
                            ],
                          ),
                        )
                      ],
                    )
                  : const Center(
                      child: CircularProgressIndicator(),
                    )),
        );
      },
    );
  }
}
