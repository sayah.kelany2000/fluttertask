import 'package:flutter/material.dart';
import 'package:task_flutter/moduels/taske/presentation/widgets/build_body_of_home.dart';


class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: BuildBodyOfHomeScreen(),
    );
  }
}
