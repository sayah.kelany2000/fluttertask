import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_flutter/moduels/taske/presentation/widgets/basic_overlay_sidget.dart';
import 'package:task_flutter/moduels/taske/service/video_app_cubit.dart';
import 'package:video_player/video_player.dart';

class LandscapePlayedScreen extends StatefulWidget {
  LandscapePlayedScreen({Key? key, required this.controller}) : super(key: key);
  VideoPlayerController controller;

  @override
  State<LandscapePlayedScreen> createState() => _LandscapePlayedScreenState();
}

class _LandscapePlayedScreenState extends State<LandscapePlayedScreen> {
  Future landscapeMode() async {
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  Future setAllOrientation() async {
    await SystemChrome.setPreferredOrientations(DeviceOrientation.values);
  }

  @override
  void initState() {
    super.initState();
    landscapeMode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    setAllOrientation();
  }

  Widget build(BuildContext context) {
    final cubit = VideoAppCubit.get(context);
    return BlocBuilder<VideoAppCubit, VideoAppState>(
      builder: (context, state) {
        return Scaffold(
          backgroundColor: Colors.black,
          body: Stack(
            children: [
              Center(
                  child: AspectRatio(
                      aspectRatio: widget.controller.value.aspectRatio,
                      child: VideoPlayer(widget.controller))),
              Positioned.fill(
                  child: BasicOverlayWidget(
                isLand: true,
                controller: cubit.controller!,
              )),
            ],
          ),
        );
      },
    );
  }
}
