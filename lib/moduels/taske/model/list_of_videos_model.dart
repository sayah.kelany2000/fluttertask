import 'package:equatable/equatable.dart';

class ListOfVideosModel extends Equatable {
  late bool success;
  late int status;
  late String message;
  List<Data> data = [];

  ListOfVideosModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    status = json['status'];
    message = json['message'];
    json['data'].forEach((element) {
      data.add(Data.fromJson(element));
    });
  }

  @override
  List<Object?> get props => [
        success,
        status,
        message,
        data,
      ];
}

class Data extends Equatable {
  late String id;
  late String name;
  late String description;
  late String rate;
  late int viewerCount;
  late int secondNumber;
  late String videoUrl;
  late String coverImage;
  late String createAt;

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    rate = json['rate'];
    viewerCount = json['viewer_count'];
    secondNumber = json['second_number'];
    videoUrl = json['video_url'];
    coverImage = json['cover_image'];
    createAt = json['createdAt'];
  }

  @override
  List<Object> get props => [
        id,
        name,
        description,
        rate,
        viewerCount,
        secondNumber,
        videoUrl,
        coverImage,
    createAt,
      ];
}
