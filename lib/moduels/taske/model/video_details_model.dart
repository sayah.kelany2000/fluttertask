import 'package:equatable/equatable.dart';
import 'package:task_flutter/moduels/taske/model/list_of_videos_model.dart';

// ignore: must_be_immutable
class VideoDetailsModel extends Equatable {
  late bool success;
  late int status;
  late String message;
 late Data dataDetails;

  VideoDetailsModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    status = json['status'];
    message = json['message'];
    dataDetails=Data.fromJson(json['data']);
  }

  @override
  List<Object?> get props => [
        success,
        status,
        message,
        dataDetails,
      ];
}


