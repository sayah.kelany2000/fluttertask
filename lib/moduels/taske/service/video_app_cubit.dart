import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_flutter/core/error/exception.dart';
import 'package:task_flutter/core/network/end_points.dart';
import 'package:task_flutter/core/network/error_model.dart';
import 'package:task_flutter/core/utils/const/strings_const/string_const.dart';
import 'package:task_flutter/moduels/taske/model/list_of_videos_model.dart';
import 'package:task_flutter/moduels/taske/model/video_details_model.dart';
import 'package:video_player/video_player.dart';

part 'video_app_state.dart';

class VideoAppCubit extends Cubit<VideoAppState> {
  VideoAppCubit() : super(VideoAppInitial());

  static VideoAppCubit get(BuildContext context) => BlocProvider.of(context);
  ListOfVideosModel? listOfVideosModel;
  int currentPage=1;
  int pageSize=5;
  void getAllVideos({bool isRefresh = false}) async {
    emit(GetAllVideoLoadingState());
    try {
      final response = await Dio().get('$url$videos',
          queryParameters: {'pageNumber': currentPage, 'pageSize': pageSize});
      if (response.statusCode == 200) {
        if (isRefresh) {
          listOfVideosModel = ListOfVideosModel.fromJson(response.data);
          emit(GetAllVideoSuccessState(listOfVideosModel!));
        } else {
          print(currentPage);
          listOfVideosModel!.data.addAll(ListOfVideosModel.fromJson(response.data).data);
          emit(GetAllVideoSuccessState(listOfVideosModel!));
        }
        currentPage++;
      } else {
        emit(GetAllVideoErrorState(
            errorModel: ErrorModel.fromJson(response.data)));
        throw ServerException(errorModel: ErrorModel.fromJson(response.data));
      }
    } catch (error) {
      emit(GetAllVideoErrorState(error: error.toString()));
      //throw ServerException(error: error.toString());
    }
  }
  VideoPlayerController ?controller;
  void initController({required String dataSource})
  {
    controller=VideoPlayerController.network('$urlPhotos''$dataSource')..addListener(()=>emit(InitVideoState()))..setLooping(true)..initialize().then((value) => controller!.play());
  }
  VideoDetailsModel? detailsModel;
  void getVideoDetails(String path) async {
    try {
      emit(GetVideoDetailsLoadingState());
      final response = await Dio().get('$url$videos/$path',);
      if (response.statusCode == 200) {
        detailsModel = VideoDetailsModel.fromJson(response.data);
        initController(dataSource: detailsModel!.dataDetails.videoUrl);

        emit(GetVideoDetailsSuccessState());
      } else {
        emit(GetVideoDetailsErrorState());
        throw ServerException(errorModel: ErrorModel.fromJson(response.data));
      }
    } catch (error) {
      emit(GetVideoDetailsErrorState());
      //throw ServerException(error: error.toString());
    }
  }
}
