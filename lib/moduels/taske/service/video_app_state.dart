part of 'video_app_cubit.dart';

@immutable
abstract class VideoAppState {}

class VideoAppInitial extends VideoAppState {}
class GetAllVideoSuccessState extends VideoAppState{
  ListOfVideosModel listOfVideosModel;
  GetAllVideoSuccessState(this.listOfVideosModel);
}
class GetAllVideoLoadingState extends VideoAppState{


}
class GetAllVideoErrorState extends VideoAppState{
  ErrorModel? errorModel;
  String? error;
  GetAllVideoErrorState({this.errorModel,this.error});
}
class InitVideoState extends VideoAppState{}

class GetVideoDetailsSuccessState extends VideoAppState{}
class GetVideoDetailsLoadingState extends VideoAppState{}
class GetVideoDetailsErrorState extends VideoAppState{}