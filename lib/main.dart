import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_flutter/core/utils/app_color.dart';
import 'package:task_flutter/moduels/taske/presentation/screens/home.dart';
import 'package:task_flutter/moduels/taske/service/video_app_cubit.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) =>
              VideoAppCubit()..getAllVideos(isRefresh: true),
            ),
          ],
          child: MaterialApp(
            theme: ThemeData(
               colorScheme: ColorScheme.fromSeed(seedColor: AppColor.primaryColor),
              primaryColor: AppColor.primaryColor,
              cardTheme:const CardTheme(
                surfaceTintColor: Colors.white
              ),
              useMaterial3: true,
            ),
            debugShowCheckedModeBanner: false,
            home: const HomeScreen(),
          ),
        );
      },
    );
  }
}
