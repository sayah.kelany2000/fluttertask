import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Components {
  static Widget defaultText(
          {required String text, required double fontSize, Color? color}) =>
      Text(
        text,
        style: TextStyle(fontSize: fontSize.sp, color: color),
      );

  static Widget cachedNetworkImage({required String imageUrl,required Widget placeHolder,required double height,required Widget errorWidget}) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context,url)=>placeHolder,
        imageBuilder: (context, imageProvider) =>  Container(
          clipBehavior: Clip.antiAliasWithSaveLayer,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              )),
          child: Image(
            fit: BoxFit.cover,
            image: imageProvider,
            height: height,
            width: double.infinity,
          ),
        ),
        errorWidget: (context, url, error) =>errorWidget ,
      );
}
