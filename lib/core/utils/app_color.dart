import 'dart:ui';

class AppColor{
  static Color primaryColor=const Color(0xff4FBDBC);
  static Color textFormColor=const Color(0xffF0F0F0);
}