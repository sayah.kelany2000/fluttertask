import 'package:task_flutter/core/network/error_model.dart';

class ServerException implements Exception
{
  final ErrorModel? errorModel;
  final String? error;
  const ServerException({this.errorModel,this.error});
}